require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr == []
    return 0
  else
    arr.reduce(:+)
  end
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  result = []
  long_strings.each do |el|
    if el.include?(substring)
      result << el
    end
  end
  if result == long_strings
    return true
  else
    return false
  end
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  hsh = Hash.new(0)
  string.chars.each do |el|
    hsh[el] += 1
  end
  k = hsh.select {|k, v| v >= 2}.keys
  k.delete(" ")
  k
end


# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.gsub!(/[[:punct:]]/, '')
  words = string.split
  longest = words.max(2) {|a, b| a.length <=> b.length }
  [longest[1], longest[0]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alph = ("a".."z").to_a
  letters = string.chars
  alph - letters
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  arr = (first_yr..last_yr).to_a
  result = []
  arr.each do |el|
    if not_repeat_year?(el)
      result << el
    end
  end
  result
end

def not_repeat_year?(year)
  if year.to_s.chars == year.to_s.chars.uniq
    return true
  else
    return false
  end
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  result = []
  songs.each do |el|
    if no_repeats?(el, songs)
      result << el
    end
  end
  result.uniq
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |el, i|
    if el == song_name
      if el == songs[i + 1]
        return false
      end
    end
  end
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.gsub!(/[[:punct:]]/, "")
  words = string.split
  hsh = Hash.new(0)
  words.each do |el|
    hsh[el] = el.rindex("c") unless el.rindex("c") == nil
  end
  sorted = hsh.sort_by { |k, v| v}
  first = sorted[0]
  first[0]
end


def c_distance(word)
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  answer = []
  chunk = []
  arr.each_with_index do |el, i|
    if el == arr[i + 1]
      chunk << i
    elsif el != arr[i + 1]
      chunk << i
      answer << chunk
      chunk = []
    end
  end
  ranges = answer.reject { |el| el.length == 1 }
  ranges.map do |el|
    el = [el.min, el.max]
  end 
end
